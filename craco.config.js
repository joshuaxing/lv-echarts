/*
 * @Author: your name
 * @Date: 2020-11-17 09:53:42
 * @LastEditTime: 2020-11-17 12:01:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \sso-admin\craco.config.js
 */
const CracoLessPlugin = require("craco-less");
const resolve = dir => require('path').join(__dirname, dir)
module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: { "@primary-color": "#1890ff" },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
  babel: {
    plugins: [
      [
        "@babel/plugin-proposal-decorators",
        {
          legacy: true,
        },
      ],
      [
        "@babel/plugin-proposal-class-properties",
        {
          loose: true,
        },
      ],
      [
        "import",
        {
          libraryName: "antd",
          libraryDirectory: "lib",
          style: true
        },
        "antd"
      ],
    ],
  },
  webpack: {
    alias: {
      '@': resolve('src')
    },
  }
};
