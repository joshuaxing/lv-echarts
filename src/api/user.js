/*
 * @Author: your name
 * @Date: 2020-09-28 11:29:10
 * @LastEditTime: 2020-12-30 11:39:30
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \sso-admin\src\api\user.js
 */
import request from "@/utils/http";
const baseURL = "";
// 登录
export function login(data) {
  return request.post(`${baseURL}/api/staff/login`, data);
}

// 退出登录
export function loginout(data) {
  return request.post(`${baseURL}/api/staff/loginout`, data);
}

// 根据用户ID查询系统模块
export function systemlist(data) {
  return request.post(`${baseURL}/api/systemlist/systemlist`, data);
}

// 获取授权字符串和工号或用户名
export function authorized(data) {
  return request.post(`${baseURL}/api/systemlist/authorized`, data);
}

// hr系统跳转
export function systemhr(data) {
  return request.post(`${baseURL}/api/hr/loginurl`, data);
}

// 修改中台账号密码以及sis鉴权系统密码
export function modifyPwd(data) {
  return request.post(`${baseURL}/api/shijiupdateuser/pwd`, data);
}


// 石基系统跳转
export function systemshiji(data) {
  return request.post(`${baseURL}/api/shiji/loginurl`, data);
}

// 获取石基系统token
export function shijitoken(data) {
  return request.post(`${baseURL}/api/shiji/redirect`, data);
}

// 激活账号成功后，记录密码
export function shijisynpwd(data) {
  return request.post(`${baseURL}/api/shiji/synpwd`, data);
}

// 系统的详情
export function systemDetail(data) {
  return request.post(`${baseURL}/api/shiji/redirect`, data);
}

// 石基账户是否激活
export function isUpdateSucess(data) {
  return request.post(`${baseURL}/api/shijicreateuser/isUpdateSucess`, data);
}

// 退出石基登录
export function shijiLoginOut(data) {
  return request.post(`${baseURL}/api/shijicreateuser/loginout`, data);
}


// 判断是否退出
export function loginOutCheck(data) {
  return request.post(`${baseURL}/api/shijicreateuser/isLoginout`, data);
}
