import React, { useState, useEffect } from "react";
import './style.less'
import * as api from "@/api/user";
import { message, Modal, Form, Input, Alert } from "antd";

const PasswordCom = ({ callback, initvisible, ispage, user, successCallBack, errorCallBack}) => {

    const [form] = Form.useForm();
    const [visible, setVisible] = useState(initvisible);
  
    useEffect(() => {
      // Update the document title using the browser API
      setVisible(initvisible);
    });

    // console.log(user)
    
    const hanldeEdit = (values) => {
      const params = {
        phone: user.phone,
        newpwd: values.password,
        oldpwd: values.oldpassword
      };
      api.modifyPwd({ data: params }).then((result) => {
        callback();
        if (result.code === 0) {
          //修改成功
          message.success('密码修改成功');
          successCallBack();
        } else {
            // ispage 1 === 我的页面修改密码  0 === 登录页修改密码-
            if (parseInt(ispage) === 1) {
              message.error(result.msg);
            } else {
              // errorCallBack();
            }
        }
      });
    };
  
    const handleOk = () => {
      form.validateFields()
      .then(values => {
        const { password, surepassword} = values
        const reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@!~%^&*\.])[a-zA-Z\d$@!~%^&*\.]{8,64}$/
        if (!reg.test(password)) {
          message.error('新密码长度不能小于8位大于64位，必须至少包含1个特殊字符($@!~%^&*.)，1个大写字母， 1个小写字母，1个数字');
        } else if (password !== surepassword) {
          message.error('新密码和确认密码不一致');
        } else {
          hanldeEdit(values)
        }
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
    };

    const handleCancel = () => {
      form.resetFields();
      callback();
    };
  
    return (
      <Modal
        title="修改密码"
        visible={visible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form
          labelCol={{ span: 4 }}
          form={form}
          name="form-hooks"
          labelAlign="right"
          layout="horizontal"
        >
          <Form.Item
            label="旧密码"
            name="oldpassword"
            rules={[{ required: true, message: "请输入..." }]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label="新密码"
            name="password"
            rules={[{ required: true, message: "请输入..." }]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label="确认密码"
            name="surepassword"
            rules={[{ required: true, message: "请输入..." }]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
        <Alert message="新密码规则：（长度不能小于8位大于64位，必须至少包含1个特殊字符($@!~%^&*.)，1个大写字母， 1个小写字母，1个数字）" type="warning" />
      </Modal>
    );
  };
  
  export default PasswordCom