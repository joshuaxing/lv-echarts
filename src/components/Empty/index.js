/*
 * @Author: your name
 * @Date: 2020-09-29 10:37:33
 * @LastEditTime: 2020-09-29 10:42:46
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \sso-admin\src\components\Empty\index.js
 */
import React, { Component } from "react";
import { Empty } from "antd";
import './style.less'
function EmptyData () {
  return (
    <div className="empty-box">
        <Empty/>
    </div>
  )
}

export default EmptyData