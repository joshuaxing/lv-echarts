/*
 * @Author: your name
 * @Date: 2020-09-23 15:47:41
 * @LastEditTime: 2020-12-30 11:01:24
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \sso-admin\src\components\HeaderBar\index.js
 */
import React, { Component, useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import "./style.less";
import * as api from "@/api/user";
import { Dropdown, Menu, Row, Col, message, Modal, Form, Input } from "antd";
import screenfull from "screenfull";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "@/store/actions";
import {
  UserOutlined,
  LogoutOutlined,
  ArrowsAltOutlined,
  ShrinkOutlined,
  PhoneOutlined,
  LockOutlined,
} from "@ant-design/icons";
import { clearUserToken } from "@/utils/gotToken";
import avatar from "./img/avatar.jpg";

import PasswordCom from '../EditPassWord/index'
@connect(
  (state) => state.user,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class HeaderBar extends Component {
  state = {
    isFullscreen: 1,
    visible: false,
  };

  componentDidMount() {
    screenfull.onchange(() => {
      const isFullscreen = screenfull.isFullscreen ? 2 : 1;
      this.setState({
        isFullscreen: isFullscreen,
      });
    });
  }

  componentWillUnmount() {
    screenfull.off("change");
  }

  toggle = () => {
    this.props.onToggle();
  };

  screenfullToggle = () => {
    if (screenfull.enabled) {
      screenfull.toggle();
    }
  };

  logout = () => {
    const params = {};
    api.loginout({ data: params }).then((result) => {
      if (result.code === 0) {
        //清除登录态
        clearUserToken();
        this.props.Logout();
        this.props.history.replace("/login");
      } else {
        message.error(result.msg);
      }
    });
    // 退出石基登录
    this.shijiLoginOut();
  };

  shijiLoginOut () {
    const { user } = this.props;
    const params = {
      phone: user.phone
    }
    api.shijiLoginOut({ data: params }).then((result) => {
      if (result.code === 0) {
        console.log('退出石基登录');
      } else {
        console.log(result.msg);
        // message.error(result.msg);
      }
    });
  }

  handleEdit = () => {
    this.setState({
      visible: true
    });
  };

  handleClose = () => {
    this.setState({
      visible: false
    });
  };

  render() {
    const { user } = this.props;
    const menu = (
      <Menu className="menu">
        <Menu.Item>
          <PhoneOutlined />
          <span>{user.phone}</span>
        </Menu.Item>
        <Menu.Item>
          <LockOutlined />
          <span onClick={this.handleEdit}>修改密码</span>
        </Menu.Item>
        <Menu.Item>
          <LogoutOutlined />
          <span onClick={this.logout}>退出登录</span>
        </Menu.Item>
      </Menu>
    );

    return (
      <div id="headerbar">
        <Row>
          <Col span={12}></Col>
          <Col span={12}>
            <div style={{ lineHeight: "64px", float: "right" }}>
              <ul className="header-ul">
                <li>
                  <Dropdown overlay={menu}>
                    <div>
                      <span style={{ marginRight: "10px" }}>
                        您好，{user.truename}
                      </span>
                      <img src={avatar} alt="" />
                    </div>
                  </Dropdown>
                </li>
              </ul>
            </div>
          </Col>
        </Row>
        <PasswordCom
          callback={this.handleClose}
          initvisible={this.state.visible}
          successCallBack={this.logout}
          ispage="1"
          user={this.props.user}
        />
      </div>
    );
  }
}

export default withRouter(HeaderBar);
