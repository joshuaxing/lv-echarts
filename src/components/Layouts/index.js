/*
 * @Author: your name
 * @Date: 2020-09-22 12:01:15
 * @LastEditTime: 2020-09-23 16:08:34
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \sso-admin\src\components\Layouts\index.js
 */
import React, { Component } from "react";
import "./style.less";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "@/store/actions";
import { Spin, Layout } from "antd";
import HeaderBar from '@/components/HeaderBar'
const { Sider, Header, Content, Footer } = Layout;


@connect(
  (state) => state.user,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class MainLayout extends Component {
  state = {
    collapsed: false
  }
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }
  render() {
    const { children } = this.props;
    return (
      <div className="page app-page">
        
        <Layout>
          <Header style={{ background: "#fff", padding: "0 16px" }}>
            <HeaderBar
              collapsed={this.state.collapsed}
              onToggle={this.toggle}
            />
          </Header>
          <Content>
            <div className="app-page-body">{children}</div>
          </Content>
          <Footer style={{textAlign: 'center'}}>AdminManagement ©2020 Created by 远洲信息</Footer>
        </Layout>
        
      </div>
    );
  }
}
export default MainLayout;
