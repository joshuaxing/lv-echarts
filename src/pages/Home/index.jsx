/*
 * @Author: your name
 * @Date: 2020-09-22 16:05:16
 * @LastEditTime: 2020-12-31 17:16:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \sso-admin\src\page\Index\index.js
 */
import React, { Component } from "react";
import { Card, Col, Row, message } from "antd";
import "./style.less";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "@/store/actions";
import * as api from "@/api/user";
import EmptyData from "@/components/Empty";
@connect(
  (state) => state.user,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class Index extends Component {
  constructor(props) {
    super(props);
    this.iframe = React.createRef();
  }
  iframeloadcount = 0;
  state = {
    result: [],
    shijisrc: "",
    username: "",
    password: "",
    credentialId: "",
  };

  componentDidMount() {
    window.sessionStorage.removeItem("shijilinkurl")
    this.gotData();
  }

  gotData() {
    const { user } = this.props;
    const params = {
      staffid: user.userId,
    };
    api.systemlist({ data: params }).then((result) => {
      if (result.code === 0) {
        const data = result.data;
        this.setState({
          result: data,
        });
      } else {
        message.error(result.msg);
      }
    });
  }

  handleCard = (item) => {
    const { user } = this.props;
    let params = {};
    let ajax = null;
    if (item.systemno === "S001") {
      // 远洲中台系统
      ajax = api.authorized;
      params = {
        staffid: user.userId,
        systemid: item.id
      };
    } else if (item.systemno === "S002") {
      // hr系统
      ajax = api.systemhr;
      params = {
        phone: user.phone
      };
    } else if (
      item.systemno === "S003" ||
      item.systemno === "S004" ||
      item.systemno === "S005" ||
      item.systemno === "S006" ||
      item.systemno === "S007" ||
      item.systemno === "S008" ||
      item.systemno === "S009" ||
      item.systemno === "S010" ||
      item.systemno === "S018"
    ) {
      // 石基系统
      ajax = api.systemshiji;
      params = {
        phone: user.phone,
        systemno: item.systemno,
      };
      this.iframeloadcount = 0;
    } else {
      ajax = api.authorized;
      params = {
        staffid: user.userId,
        systemid: item.id
      };
    }
    if (ajax) {
      ajax({ data: params }).then((result) => {
        if (result.code === 0) {
          // 商城系统?sessionid=799dda91fdf945b8a9f48d90e2a55183&user=17521506188
          const data = result.data;
          const linkurl = data.linkurl ? data.linkurl : "";
          if (linkurl) {
            if (
              item.systemno === "S003" ||
              item.systemno === "S004" ||
              item.systemno === "S005" ||
              item.systemno === "S006" ||
              item.systemno === "S007" ||
              item.systemno === "S008" ||
              item.systemno === "S009" ||
              item.systemno === "S010" ||
              item.systemno === "S018"
            ) {

              // 石基系统
              this.setState({
                shijisrc: linkurl,
                username: data.username,
                password: data.password
              });
              sessionStorage.setItem('shijilinkurl', linkurl);
              
            } else {
              window.open(data.linkurl);
            }
          } else {
            message.warning("缺少配置跳转url");
          }
        } else {
          message.error(result.msg);
        }
      });
    } else {
      message.warning("抱歉，此系统还没有对接，请联系管理员");
    }
  };

  iframeLoad() {
    
    console.log("iframeLoad");
    console.log("次数" + this.iframeloadcount);

    


    if (this.iframeloadcount === 0) {
      
      console.log("iframeloadfirst");

      this.handlePostMessage();

      this.iframeloadcount++;
      
      // 点击了石基系统
      // localStorage.setItem('shiji', 'yzlogin');

    } else {

      console.log("iframeloadother");
      
      if (window.sessionStorage.getItem('shijiactivate')) {
        // 退出成功，再次激活
        console.log('退出成功，再次激活');
        this.handlePostMessage();
        window.sessionStorage.removeItem('shijiactivate');
      }

    }
  }


  handlePostMessage () {


    console.log('handlePostMessage')
    const { username, password, shijisrc } = this.state;
    const json = {
      username: username,
      password: password,
      submitForm: "yes"
    };

    // console.log(this.iframe.current);

    this.iframe.current.contentWindow.postMessage(JSON.stringify(json), "*");

    let passwordConfirm = "";
    const urlstr = this.getQueryString(shijisrc, "state");
    if (urlstr) {
      passwordConfirm = urlstr.split("+")[1];
    }

    // 修改密码
    const json2 = {
      passwordConfirm: passwordConfirm,
      password: passwordConfirm,
      submitForm: "yes",
    };
    
    // var timer = setInterval(() => {
    //   this.iframe.current.contentWindow.postMessage(
    //     JSON.stringify(json2),
    //     "*"
    //   );
    //   clearInterval(timer);
    // }, 2000);
    
  }

  getQueryString(url, name) {
    if (url.indexOf("?") > -1) {
      const index = url.indexOf("?");
      var query = decodeURI(url.substring(index + 1));
      var vars = query.split("&");
      for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] === name) {
          return pair[1];
        }
      }
      return null;
    }
    return null;
  }

  render() {
    const { result, shijisrc } = this.state;
    const cardTile = (item) => {
      const { logourl, systemname } = item;
      return (
        <div className="card-title-wrapper">
          <img src={logourl} className="card-title-icon" alt=" " />
          <span className="card-title">{systemname}</span>
        </div>
      );
    };

    return (
      <div className="site-card-wrapper">
        {result.length > 0 ? (
          <div className="site-card">
            <Row gutter={[16, 16]}>
              {result.map((item, index) => (
                <Col
                  span={8}
                  key={String(index)}
                  onClick={this.handleCard.bind(this, item)}
                >
                  <Card
                    title={cardTile(item)}
                    bordered={false}
                    hoverable
                    bodyStyle={{ height: "150px" }}
                  >
                    {item.description}
                  </Card>
                </Col>
              ))}
            </Row>
          </div>
        ) : (
          <EmptyData />
        )}
        {/* iframe */}
        {shijisrc && (
          <div className="fixed-box">
            <iframe
              title=" "
              src={shijisrc}
              onLoad={this.iframeLoad.bind(this)}
              width="200px"
              frameBorder="0"
              height="200px"
              ref={this.iframe}
              id="iframeId"
            ></iframe>
          </div>
        )}
      </div>
    );
  }
}

export default Index;
