import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import "./style.less";
import * as actions from "@/store/actions";
import { useDispatch } from 'react-redux'
import * as api from "@/api/user";
import { Form, Input, Button, notification, Row, Col, message} from "antd";
import { randomNum, calculateWidth, preloadingImages } from "@/utils/util";
import BGParticle from "@/utils/BGParticle";
import PromptBox from "@/components/PromptBox";
import Loading from "@/components/Loading";
import bg1 from "./img/bg1.jpg";
import "animate.css";
import { setUserToken } from "@/utils/gotToken";
const url = bg1;
const LoginForm = () => {
  const [form] = Form.useForm();
  let canvasObj = null;
  let history = useHistory();
  // 聚焦失焦事件
  const [focusItem, setFocusItem] = useState(-1);
  // 设置验证码
  const [productcode, setCode] = useState("");
 
  // 生成验证码
  const createCode = () => {
    const ctx = canvasObj.getContext("2d");
    const chars = [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      "a",
      "b",
      "c",
      "d",
      "e",
      "f",
      "g",
      "h",
      "j",
      "k",
      "l",
      "m",
      "n",
      "p",
      "q",
      "r",
      "s",
      "t",
      "u",
      "v",
      "w",
      "x",
      "y",
      "z",
      "A",
      "B",
      "C",
      "D",
      "E",
      "F",
      "G",
      "H",
      "J",
      "K",
      "L",
      "M",
      "N",
      "P",
      "Q",
      "R",
      "S",
      "T",
      "U",
      "V",
      "W",
      "X",
      "Y",
      "Z",
    ];
    let code = "";
    ctx.clearRect(0, 0, 80, 39);
    for (let i = 0; i < 4; i++) {
      const char = chars[randomNum(0, 57)];
      code += char;
      ctx.font = randomNum(20, 25) + "px SimHei"; //设置字体随机大小
      ctx.fillStyle = "#D3D7F7";
      ctx.textBaseline = "middle";
      ctx.shadowOffsetX = randomNum(-3, 3);
      ctx.shadowOffsetY = randomNum(-3, 3);
      ctx.shadowBlur = randomNum(-3, 3);
      ctx.shadowColor = "rgba(0, 0, 0, 0.3)";
      let x = (80 / 5) * (i + 1);
      let y = 39 / 2;
      let deg = randomNum(-25, 25);
      /**设置旋转角度和坐标原点**/
      ctx.translate(x, y);
      ctx.rotate((deg * Math.PI) / 180);
      ctx.fillText(char, 0, 0);
      /**恢复旋转角度和坐标原点**/
      ctx.rotate((-deg * Math.PI) / 180);
      ctx.translate(-x, -y);
    }
    setCode(code);
  };

  useEffect(() => {
    createCode();
  }, []);

  // 输入框校验
  const [checkName, setCheckName] = useState(false);
  const [checkPass, setCheckPass] = useState(false);
  const [checkCode, setCheckCode] = useState(false);
  const onChangeName = async (param, e) => {
    const value = e.target.value;
    const error = value !== '' ? false : true;
    try {
      await form.validateFields([param]);
      handleCheck(param, error);
    } catch (errorInfo) {
      handleCheck(param, error);
    }
  };

  const handleCheck = (param, error) => {
    switch (param) {
      case "username":
        setCheckName(error);
        break;
      case "password":
        setCheckPass(error);
        break;
      case "code":
        setCheckCode(error);
        break;
      default:
    }
  };

  const onFinishFailed = (values) => {
    // console.log(values);
    const errorFields = values.errorFields;
    errorFields.forEach((item) => {
      handleCheck(item.name[0], true);
    });
  };

  const loginSubmit = async (params) => {
    // 17521506188 yuanzhou123
    try {
      const result = await api
      .login({ data: params })

      if (result.code === 0) {
        const data = result.data;
        const value = {
          userId: data.staffid,
          token: data.token,
          truename: data.username,
          jobnum: data.empno,
          phone: data.phone
        }
        replaceLogin(value)
      } else {
        message.error(result.msg);
      }
    } catch (error) {
      console.log(error)
    }
  }

  const onFinish = (values) => {
    const { username, password, code } = values;
    if (code.toLowerCase() !== productcode.toLowerCase()) {
      // setCheckCode(true);
      message.error('验证码输入错误');
      createCode();
    } else {
      const params = {
        empNo: "",
        password: password,
        phone: username,
      };
      loginSubmit(params)
    }
  };
  const dispatch = useDispatch()
  const handleOpen = () => {
    const value = {
      userId: 6666,
      token: '',
      truename: '',
      jobnum: '',
      phone: ''
    }
    dispatch({
      type: 'Login',
      payload: value
    })
    history.replace('/test')
  };

  const replaceLogin = (value) => {
    setUserToken(JSON.stringify(value))
    //判断是否登陆
    // Login(value)
    history.replace('/index')
  }

  return (
    <div className="box showBox">
      <h3 className="title">统一门户登录平台</h3>
      <Form
        form={form}
        name="form-hooks"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: "请输入用户名",
            },
          ]}
          help={
            checkName && (
              <PromptBox
                info={form.getFieldError(["username"])}
                width={calculateWidth(form.getFieldError(["username"]))}
              />
            )
          }
          
        >
          <Input
            onChange={(e) => onChangeName("username", e)}
            onFocus={() => setFocusItem(0)}
            onBlur={() => setFocusItem(-1)}
            maxLength={16}
            placeholder="用户名"
            addonBefore={
              <span
                className="iconfont icon-User"
                style={focusItem === 0 ? styles.focus : {}}
              />
            }
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "请输入密码",
            },
          ]}
          help={
            checkPass && (
              <PromptBox
                info={form.getFieldError(["password"])}
                width={calculateWidth(form.getFieldError(["password"]))}
              />
            )
          }
        >
          <Input
            onChange={(e) => onChangeName("password", e)}
            onFocus={() => setFocusItem(1)}
            onBlur={() => setFocusItem(-1)}
            placeholder="密码"
            type="password"
            addonBefore={
              <span
                className="iconfont icon-suo1"
                style={focusItem === 1 ? styles.focus : {}}
              />
            }
          />
        </Form.Item>
        <Form.Item
          name="code"
          rules={[
            {
              required: true,
              message: "请输入验证码",
            },
          ]}
          help={
            checkCode && (
              <PromptBox
                info={form.getFieldError(["code"])}
                width={calculateWidth(form.getFieldError(["code"]))}
              />
            )
          }
        >
          <Row>
            <Col span={15}>
              <Input
                onChange={(e) => onChangeName("code",e)}
                onFocus={() => setFocusItem(2)}
                onBlur={() => setFocusItem(-1)}
                placeholder="验证码"
                addonBefore={
                  <span
                    className="iconfont icon-securityCode-b"
                    style={focusItem === 2 ? styles.focus : {}}
                  />
                }
              />
            </Col>
            <Col span={9}>
              <canvas
                onClick={createCode}
                width="80"
                height="39"
                ref={(el) => (canvasObj = el)}
              />
            </Col>
          </Row>
        </Form.Item>
        <Form.Item>
          {/* <Button type="primary" htmlType="submit">
            登录
          </Button> */}
          <Button type="primary" onClick={handleOpen}>
            登录
          </Button>
        </Form.Item>
      </Form>
      <div className="footer">
        <div>欢迎登录统一门户登录平台</div>
      </div>
    </div>
  );
};




const Login = () => {
  let particle = null;
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    ininData()
    return () => {
      particle && particle.destory();
      notification.destroy();
      particle = null;
    }
  }, [])

  const ininData = async() => {
    try {
      await loadImageAsync(url)
      setLoading(false)
      particle = new BGParticle("backgroundBox");
      particle.init();
    } catch (error) {
      console.log(error)
    }
  }

  //登录的背景图太大，等载入完后再显示，实际上是图片预加载，
  const loadImageAsync = (url) => {
    return new Promise(function (resolve, reject) {
      const image = new Image();
      image.onload = function () {
        resolve(url);
      };
      image.onerror = function () {
        reject("图片载入错误");
      };
      image.src = url;
    });
  }

  return (
    <div id="login-page">
      {loading ? (
        <div>
          <h3 style={styles.loadingTitle} className="animated bounceInLeft">
            载入中...
          </h3>
          <Loading />
        </div>
      ) : (
        <div>
          <div id="backgroundBox" style={styles.backgroundBox}></div>
          <div className="container">
            <LoginForm/>
          </div>
        </div>
      )}
    </div>
  );
}

const styles = {
  backgroundBox: {
    position: "fixed",
    top: "0",
    left: "0",
    width: "100vw",
    height: "100vh",
    backgroundImage: `url(${url})`,
    backgroundSize: "100% 100%",
    transition: "all .5s",
  },
  focus: {
    width: "20px",
    opacity: 1
  },
  loadingBox: {
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
  },
  loadingTitle: {
    position: "fixed",
    top: "50%",
    left: "50%",
    marginLeft: -45,
    marginTop: -18,
    color: "#000",
    fontWeight: 500,
    fontSize: 24,
  },
};

export default Login;
