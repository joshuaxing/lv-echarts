import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'

function Test() {
   const user = useSelector(state =>
     {
       return state.user.user
     }
   )
   // console.log(user)
   useEffect(() => {
      // console.log(user)
   })
   return (
      <div>Test{user.userId}</div>
   )
}

export default Test