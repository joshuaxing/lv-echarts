
import MainLayout from '@/components/Layouts';
import LoadableComponent from '@/components/LoadableComponent'

const Home = LoadableComponent(() => import('@/pages/Home'));
const Login = LoadableComponent(() => import('@/pages/Login'));
const Test = LoadableComponent(() => import('@/pages/Test'));
const Index = LoadableComponent(() => import('@/pages/Index'));
const defaultRoutes = [
    {
        path:'/login',
        component: Login
    },
    {
        path: '/home',
        component: Home
    },
    {
        path: '/test',
        component: Test
    },
    {
        path: '/index',
        component: Index
    },
    {
        path: '/',
        redirect: '/index'
    },
]

const adminRoutes = [
 
]

export default {
  default: defaultRoutes,
  admin: adminRoutes
}