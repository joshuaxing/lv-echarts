/*
 * @Author: your name
 * @Date: 2020-03-03 11:19:50
 * @LastEditTime: 2020-09-29 09:16:41
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\store\actions.js
 */

// 登录
export const Login = (data, callback) => async(dispatch, getState) => {
  dispatch({
    type: 'Login',
    payload: data
  });
  callback && callback();
}

export const Logout = (data, callback) => async(dispatch, getState) => {
  const payload = {
    userId: 0,
    token: '',
    truename: '',
    jobnum: '',
    phone: ''
  }
  dispatch({
    type: 'Login',
    payload: payload
  });
  callback && callback();
}

