/*
 * @Author: your name
 * @Date: 2020-03-03 11:58:16
 * @LastEditTime: 2020-11-17 13:40:09
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\utils\http.js
 */
import axios from "axios";
import configureStore from "@/store";
// import { history } from "@/router";
import { gotUserToken, clearUserToken } from "@/utils/gotToken";
import { message } from "antd";
const store = configureStore();
//请求的公共参数
let arg = {};
//定义全部http
let http = {};

// Content-Type: application/json
http.post = function (api, params) {
  if (gotUserToken()) {
    arg.token = JSON.parse(gotUserToken()).token;
  } else {
    arg.token = "";
  }
  arg.actcode = "";
  arg.appcode = "";
  arg.modcode = "";
  const data = Object.assign({}, arg, params);

  // Toast.loading('数据加载中...');
  return new Promise((resolve, reject) => {
    axios({
      method: "post",
      url: api,
      data: data,
      header: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        // console.log(response);
        if (response.status === 200) {
          const data = response.data;
          if (data.code === 401) {
            // token过期
            message.warning("您的账号已过期，请重新登录");
            // store.dispatch({
            //   type: "Logout",
            // });
            // 清除缓存token
            clearUserToken();
            // history.push("/login");
          } else {
            resolve(response.data);
          }
        } else {
          message.error(response.status);
        }
      })
      .catch((error) => {
        message.error(error.message);
      });
  });
};




const key = 'keyhttp';

function startLoading() {
  //使用Element loading-start 方法
  console.log('startLoading')
  message.loading({ content: 'Loading...', key });
}

function endLoading() {
  //使用Element loading-close 方法
  console.log('endLoading')
  message.destroy();
}


//showFullScreenLoading() tryHideFullScreenLoading() 用于将同一时刻的请求合并。
//声明一个变量 needLoadingRequestCount，每次调用showFullScreenLoading方法 needLoadingRequestCount + 1
//调用tryHideFullScreenLoading()方法，needLoadingRequestCount - 1   needLoadingRequestCount为 0 时，结束 loading

let needLoadingRequestCount = 0;
function showFullScreenLoading() {
  if (needLoadingRequestCount === 0) {
    startLoading();
  }
  needLoadingRequestCount++;
}

function tryHideFullScreenLoading() {
  if (needLoadingRequestCount <= 0) return;
  needLoadingRequestCount--;
  if (needLoadingRequestCount === 0) {
    endLoading();
  }
}


// http request 拦截器
axios.interceptors.request.use(
  (config) => {
    showFullScreenLoading()
    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);

// http response 拦截器
axios.interceptors.response.use(
  (response) => {
    tryHideFullScreenLoading();
    return response;
  },
  (error) => {
    tryHideFullScreenLoading();
    if (error && error.response) {
      switch (error.response.status) {
        case 400:
          error.message = "请求错误(400)";
          break;
        case 403:
          error.message = "拒绝访问(403)";
          break;
        case 404:
          error.message = "请求出错(404)";
          break;
        case 408:
          error.message = "请求超时(408)";
          break;
        case 500:
          error.message = "服务器错误(500)";
          break;
        case 501:
          error.message = "服务未实现(501)";
          break;
        case 502:
          error.message = "网络错误(502)";
          break;
        case 503:
          error.message = "服务不可用(503)";
          break;
        case 504:
          error.message = "网络超时(504)";
          break;
        case 505:
          error.message = "HTTP版本不受支持(505)";
          break;
        default:
          error.message = `连接出错(${error.response.status})!`;
      }
      return Promise.reject(error);
    } else {
      error.message = "网络出错, 请稍后重试";
      return Promise.reject(error);
    }
  }
);

export default http;
